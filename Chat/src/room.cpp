#include "room.h"
#include "server.h"
#include <typeinfo>

using namespace std;

Server* server;

Room::Room()
{
  cout << this << endl;
  this->name = (char *) malloc(MAX_NAME_LENGHT+1);
  this->clientCount = 0;
  this->messageCount = 0;
}

void Room::SetName(const char *name)
{
  //Copies at most MAX_NAME_LENGHT + 1 (including '\0')
  snprintf(this->name, MAX_NAME_LENGHT+1, name);
}

void Room::SetId(int id)
{
  this->id = id;
}

void Room::Start()
{
 //   this->thread = new MyThread();
    pthread_t p;
    pthread_create(&p, NULL, this->PollWrapper, (void*)this);

  //  thread->Create((void*)PollClients, NULL);
}

void* Room::PollWrapper(void* roomPtr)
{
    ((Room*)roomPtr)->PollClients();
}

void Room::PollClients()
{
    while(1)
    {
        cout << "pointer: " << this <<  "id: " << id << " client count: " << clientCount << endl;
        cout << "static server test: " << server->clients.size() << endl;
        cout << "roomId: " << this->id<< " number of messges test: " << this->Messages.size() << endl;

        char buffer[256-25];

        int result;
        //check clients status
        result = poll(this->fds, clientCount, POLL_TIMEOUT);

        if (result < 0)
        {
            perror("poll() failed");
        }
        if (result == 0)
        {
            printf("poll() timed out.\n");
        }
        for (int i = 0; i < this->clientCount; i++)
        {
            cout << "Client socket: " << fds[i].fd << endl;
            Client *c = (this->clients[i]);
            c->roomFd = i;
            int close_conn = 0;
            if (fds[i].revents & POLLERR)
            {
                printf("socket error, closing connection...\n");
                close_conn = 1;
            }
            else if (fds[i].revents & POLLIN)
            {

                char message[256];
                memset(message, 0, sizeof(message));
                cout << "Receiving message" << endl;
                //process the incoming message
                memset(buffer, 0, sizeof buffer);
                //result = recv(fds[i].fd, buffer, sizeof buffer, 0);
                int q;
                int j = 0;
                int nbuf = 0;
                char sign[1];
                cout << "sign[0]: " << sign[0] << endl;
                //char buffor[256];
                int result2 = read(fds[i].fd,sign,1);
                cout << " result: " << result << endl;




                if(result2 < 0)
                {
                    cerr << "Error while receiving message from client: " << endl;
                }
                else if(result2 == 0){
                    printf("closing connection...\n");
                    close(fds[i].fd);
                    fds[i].fd *= -1;
                }
                else if(result2 > 0)
                {
                    bool flag = true;
                    while(1){

                        message[nbuf] = sign[0];
                        cout << "nbuf: " << nbuf << " sign[0]: " << sign[0] << endl;
                        nbuf++;
                        //if(flag == false){break;}
                        read(fds[i].fd,sign,1);

                        //if(sign[0] == '\n'){flag = false;}
                        if(sign[0] == '&'){break;}
                    }
                    cout << "OUT" << endl;
                    //char *temp;
                    read(fds[i].fd,sign,1);

                    //char str[1024];
                    //char tmp = '.';

                    //strcat(str, tmp);


                    //char tmp[2] = "&";

                    //strcat(message, tmp);


                    //message[nbuf] = '&';


                    //string endline = "\n";
                    //message[nbuf] = (char) endline.c_str();


                    int result3 = nbuf;
                    cout << "Result: " << result3 << " Message: "<< message << " " << strlen(message) << endl;
                    //Message received. Send to all clients.
                    //snprintf(message, sizeof message, buffer);
                    cout << "server: " << message << endl;
                    cout << sizeof(message) << endl;
                    cout << sizeof(message[0]) << "   " << typeid(message[0]).name() << endl;
                    cout << sizeof(message) / sizeof(message[0]) << endl;

                    int processingResult = this->ProcessMessage(string(message), c);
                    if (processingResult == 0)
                    {


                    }
                    else
                    {
                        //std::string s = std::to_string(result);
                        //this->Callback(c,s.c_str());
                        switch(processingResult)
                        {
                        case -8:
                            {
                                                            char *cb = "RoomCreated\n";
                            this->Callback(c,cb);
                            break;
                            }

                        case -9:
                            break;
                        case -10:
                            {
                            char *cb = "ChangedRoom\n";
                            this->Callback(c,cb);
                            break;
                            }
                        case -11:
                            {
                                                                                        char *cb = "HistorySent\n";
                            this->Callback(c,cb);
                            break;
                            }

                        case -12:
                            {
                                                                                        char *cb = "RoomsListSent\n";
                            this->Callback(c,cb);
                            break;
                            }
                        case -13:
                            {
                                                                                        char *cb = "GetIdSent\n";
                            this->Callback(c,cb);
                            break;
                            }
                        case -14:
                            {
                                                                                        char *cb = "GetRoomIdSent\n";
                            this->Callback(c,cb);
                            break;
                            }
                        case -15:
                            {
                                                                                        char *cb = "SetClientLogin\n";
                            this->Callback(c,cb);
                            break;
                            }
                        default:
                            //jakiś kod
                            break;
                        }
                        //this->SendHistory(this->clients[i]);
                        //this->SendHistory(this->clients[i].id);
                    }
                }
                if (close_conn)
                {
                    //disconnetion handling - TODO FOR CZCIBOR
                    printf("closing connection...\n");
                    close(fds[i].fd);
                    fds[i].fd *= -1;
                    fds[i].events = 0;
                }
                //fds[i].revents = 0;
            }
        }
    }
}

char* Room::PrepareMessage(int clientId, char* message)
{
//    char *rId;
//    sprintf(rId, "%i", roomId);
//    char* delimiter= "|||";
//
//    char* full_text;
//    //cout << strlen(rId) << endl;
//    //cout << strlen(delimiter) << endl;
//    //cout << strlen(message) << endl;
//    full_text= (char*)malloc(strlen(rId)+strlen(delimiter)+strlen(message)+1);
//    strcpy(full_text, rId );
//    strcat(full_text, delimiter);
//    strcat(full_text, message);
    cout << "Prepare Message Begin: " << message << endl;

//    cout << strlen(cId) << endl;
//    cout << strlen(delimiter) << endl;
    cout << strlen(message) <<" 1msg: " << message << endl;

        const char *cId = std::to_string(clientId).c_str();
        cout << strlen(message) <<" 2msg: " << message << endl;
    //sprintf(cId, "%i", clientId);

    cout << strlen(message) <<" 3msg: " << message << endl;
    char* delimiter= "|||";
cout << strlen(message) <<" 4msg: " << message << endl;
    char* full_text;
cout << strlen(message) <<" 5msg: " << message << endl;
    full_text= (char*)malloc(strlen(cId)+strlen(delimiter)+strlen(message)+1);
    strcpy(full_text, cId );
    strcat(full_text, delimiter);
    strcat(full_text, message);

cout << "Prepare Message End: " << full_text << endl;
    return full_text;

}


int SendK(char *buffor_answer, Client* c)
{
    cout << "SendK" << endl;
    int m=0,n=0,p=0;
	char buffor[256];


    //char buffor_answer[64] = "Mikolaj Musidlowski\n";
    //n = sizeof(buffor_answer);
    n = strlen(buffor_answer);

    cout << "buffor_answer: " << buffor_answer << " sizeof(buffor_answer): " << n << endl;

    //cout << buffor_answer[3] << endl;

    while(m!=n){
        p = write(c->sock,buffor_answer+m,n-m);
        cout << "p: " << p << endl;
        if (p = -1){
            break;
        }

        m += p;
    }
    memset(buffor, 0, sizeof(buffor));
    return n;
}

void Room::Broadcast(Message* m)
{
    int n;
    //PrepareMessage(c->id,m);
    int clientId = m->clientId;
    string message = std::to_string(m->roomId) + "|||" + std::to_string(m->clientId) + "|||" + m->clientLogin + "|||" + std::to_string(m->id) + "|||" + ctime(&(m->rawtime)) + "|||" + m->content;
    //Acquire the lock
    cout << "Room::Broadcast name: " << this->name << endl;
    MyThread::LockMutex((const char *) this->name);
    for(size_t j=0; j< this->clients.size(); j++)
    {
        if(this->clients[j]->id == clientId)
        {
            char *cb = "MessageReceived";
            //n = send(this->clients[j]->sock, cb, strlen(cb), 0);
            n = SendK(cb, this->clients[j]);
            cout << n << " bytes sent." << endl;
        }
//        if(this->clients[j].id != clientId)
//        {
//            n = send(this->clients[j].sock, message.c_str(), strlen(message.c_str()), 0);
//            cout << n << " bytes sent." << endl;
//        }

        char* full_text;
        full_text= (char*)malloc(strlen(message.c_str())+1);
        strcpy(full_text, message.c_str() );

        n = SendK(full_text, this->clients[j]);
        //n = send(this->clients[j]->sock, message.c_str(), strlen(message.c_str()), 0);
        cout << n << " bytes sent." << endl;
    }
    //Release the lock
    MyThread::UnlockMutex((const char *) this->name);
    cout << "Room::Broadcast returned: " << endl;
}

void Room::Callback(Client *c, const char *message)
{
    int n;
    //Acquire the lock
    cout << "Room::Callback name: " << this->name << endl;
    MyThread::LockMutex((const char *) this->name);
            //n = send(c->sock, message, strlen(message), 0);
            n = SendK((char*)message, c);
    //Release the lock
    MyThread::UnlockMutex((const char *) this->name);
    cout << "Room::Callback returned: " << endl;
}

//void Room::SendHistory(int clientId)
//{
//    int n;
//    //Acquire the lock
//    MyThread::LockMutex("'SendToAll()'");
//    for(size_t j=0; j< this->Messages.size(); j++)
//    {
//
//            n = send(this->clients[clientId].sock, Messages[j].content.c_str(), strlen(Messages[j].content.c_str()), 0);
//            cout << n << " bytes sent." << endl;
//    }
//    //Release the lock
//    MyThread::UnlockMutex("'SendToAll()'");
//}

void Room::SendHistory(Client *c)
{
    int n;
    //Acquire the lock
    MyThread::LockMutex((const char *) this->name);
    cout << "SendingHistory" << endl;
    char* delimiter= "@@@";

    string history = "SendingHistory";
    for(size_t j=0; j< this->Messages.size(); j++)
    {
        cout << "m id: " << Messages[j].id << " clientId: " << Messages[j].clientId << " content: " << Messages[j].content << endl;
//        int id = Messages[j].id;
//        int clientId = Messages[j].clientId;
//        string tid; // brzydkie rozwiązanie
//        string tclientId;
//        sprintf((char*)tid.c_str(), "%d", id);
//        sprintf((char*)tclientId.c_str(), "%d", clientId);
//        //string str = tmp.c_str();
//        string sid = tid.c_str();
//        string sclientId = tclientId.c_str();

        string id = std::to_string(Messages[j].id);
        string clientId = std::to_string(Messages[j].clientId);

        cout << "message: " << id << "  " << clientId << "  " << endl;
        //+ std::to_string(ctime(Messages[j]->&rawtime)) + delimiter
//        printf ("The current local time is: %s", ctime(Message[j]->&rawtime));
        string message = delimiter + id + delimiter + clientId+ delimiter+Messages[j].clientLogin  +delimiter + ctime(&(Messages[j].rawtime)) + delimiter  + Messages[j].content;
        history += message;


    }
    //history += "\n";
    history+="###";
    //Release the lock
    cout << "history: " <<history<< endl;
    char* full_text;
    full_text= (char*)malloc(strlen(history.c_str())+1);
    strcpy(full_text, history.c_str() );

    //n = send(c->sock, full_text, strlen(full_text), 0);
    n = SendK(full_text, c);

    cout << n << " bytes sent." << endl;
    MyThread::UnlockMutex((const char *) this->name);
}

void Room::SendRooms(Client *c)
{
    int n;
    //Acquire the lock
    MyThread::LockMutex((const char *) this->name);

    cout << "SendingRooms" << endl;
    char* delimiter= "@@@";

    string rooms = "SendingRooms@@@";

    for(size_t j=0; j< server->rooms.size(); j++)
    {
        string id = std::to_string(server->rooms[j]->id);

        string message = id + delimiter;
        rooms += message;
    }
    rooms += "###";
    //Release the lock
    char* full_text;
    full_text= (char*)malloc(strlen(rooms.c_str())+1);
    strcpy(full_text, rooms.c_str() );

    //n = send(c->sock, full_text, strlen(full_text), 0);
    n = SendK(full_text, c);
    cout << n << " bytes sent." << endl;

    MyThread::UnlockMutex((const char *) this->name);
}

void Room::SetClientLogin(string login, Client *c)
{

    MyThread::LockMutex((const char *) this->name);

    c->SetName(login.c_str());

    MyThread::UnlockMutex((const char *) this->name);
}


void Room::GetId(Client *c)
{
    int n;
    //Acquire the lock
    MyThread::LockMutex((const char *) this->name);

    cout << "GetId" << endl;
    char* delimiter= "@@@";

    string getid = "GetId@@@";

    string id = std::to_string(c->id);
    string message = getid + id +"###";

    //Release the lock
    char* full_text;
    full_text= (char*)malloc(strlen(message.c_str())+1);
    strcpy(full_text, message.c_str() );

    //n = send(c->sock, full_text, strlen(full_text), 0);
    n = SendK(full_text, c);
    cout << n << " bytes sent." << endl;

    MyThread::UnlockMutex((const char *) this->name);
}

void Room::GetRoomId(Client *c)
{
    int n;
    //Acquire the lock
    MyThread::LockMutex((const char *) this->name);

    cout << "GetId" << endl;
    char* delimiter= "@@@";

    string getroomid = "GetRoomId@@@";

    string id = std::to_string(this->id);
    string message = getroomid + id +"###";

    //Release the lock
    char* full_text;
    full_text= (char*)malloc(strlen(message.c_str())+1);
    strcpy(full_text, message.c_str() );

    //n = send(c->sock, full_text, strlen(full_text), 0);
    n = SendK(full_text, c);
    cout << n << " bytes sent." << endl;

    MyThread::UnlockMutex((const char *) this->name);
}

Message* Room::AddMessage(Client *c, string s)
{

    MyThread::LockMutex((const char *) c->name);
    Message *m = new Message(c->id);
    m->SetId(this->Messages.size());
    m->SetContent(s);
    m->SetClientId(c->id);
    m->SetRoomId(this->id);
    m->SetClientLogin(c->name);
    //r->Message->SetClient(*c);
    cout << "ROOMID: " << m->roomId <<" CLIENTID: " << c->id << " m id: " << m->id << " clientId: " << m->clientId << " content: " << m->content << endl;
    this->Messages.push_back(*m);

    MyThread::UnlockMutex((const char *) c->name);
    return m;
}

int Room::ProcessMessage(string s, Client *c)
{
    std::string delimiter = "|||";
    std::string command = "$$$";
    std::string change_room = "___";
    size_t pos = 0;
    std::string token;
    //id
    pos = s.find(delimiter);

    //command


        if (pos == string::npos){
        //string s2 = s;
        //pos = s2.find(command);
        //s2.erase(0, pos + command.length());
        //;
        if (s.find("CreateRoom") != string::npos){
            //createrooom
            string s2 = s;
            int roomId = Room::CreateRoom();
            pos = s2.find("CreateRoom");
            cout << "CreateRoom: "<< roomId << endl;
            //server->JoinRoom(roomId,c);
            char *cb = "RoomCreated";
                            this->Callback(c,cb);
        }


//        if(s.find("JoinRoom") != string::npos){
//                    //joinroom
//                    string s2 = s;
//                    string jr = "JoinRoom";
//                    pos = s2.find("JoinRoom");
//                    s2.erase(0, pos + jr.length());
//                    int roomId = atoi( s2.c_str() );
//                    cout << "JoinRoom: "<< roomId << endl;
//                    Room::JoinRoom(roomId,c);
//                    return -9;
//                }

        if(s.find("ChangeRoom") != string::npos){
                        //joinroom
                        string s2 = s;
                        string cr = "ChangeRoom";
                        pos = s.find("ChangeRoom");
                        s.erase(0, pos + cr.length());
                        pos = s.find(command);

                        //char * pch;
                        //char *cstr = new char[s.length() + 1];
                        //strcpy(cstr, s.c_str());
                        //pch = strtok (cstr,change_room);

                        //std::vector<std::string> arr;
                        //arr=split(s,change_room);
                        string str = s;
                        string sep = change_room;
                        char* cstr=const_cast<char*>(str.c_str());
                        char* current;
                        std::vector<std::string> arr;
                        current=strtok(cstr,sep.c_str());
                        while(current!=NULL){
                            arr.push_back(current);
                            current=strtok(NULL,sep.c_str());
                        }


                        int currentRoomId = atoi( arr[0].c_str() );
                        int desiredRoomId = atoi( arr[1].c_str() );
                        cout << "ChangeRoom: "<< currentRoomId << "   " << desiredRoomId << endl;
                        Room::ChangeRoom(currentRoomId,desiredRoomId,c);
                        cout << "ROOOM CHAAAAAGNEEEDDD" << endl;
                            char *cb = "ChangedRoom";
                            this->Callback(c,cb);

                    }


        if(s.find("SendHistory") != string::npos){
                            this->SendHistory(c);
                            char *cb = "HistorySent";
                            this->Callback(c,cb);
                        }

        if(s.find("SendRooms") != string::npos){
                                this->SendRooms(c);

                            char *cb = "RoomsListSent";
                            this->Callback(c,cb);

                            }

        if(s.find("GetId") != string::npos){
                                    this->GetId(c);

                                    char *cb = "GetIdSent";
                                    this->Callback(c,cb);

                                }


        if(s.find("GetRoomId") != string::npos){
                                        this->GetRoomId(c);
                                        char *cb = "GetRoomIdSent";
                                        this->Callback(c,cb);
        }

        if(s.find("SetClientLogin") != string::npos){
                                                string s2 = s;
                                                string jr = "SetClientLogin";
                                                pos = s2.find("SetClientLogin");
                                                size_t pos2 = s2.find("&");
                                                int pos3 = s.find("&");
                                                s2 = s2.substr(pos+jr.length(),pos2);
                                                cout << "s2: " <<  s2 << " pos: " << pos << " pos2: " << pos2 <<  " pos3: " << std::to_string(pos3) <<endl;
                                                //s.erase(0, pos + jr.length());
                                                string login = s2;

                                                string deli = "&";
                                                pos = login.find(deli);
                                                cout << "final pos: " << deli << endl;
                                                s2 = s2.substr(0,pos);

                                                login = s2;

                                                cout << "Login: "<< s << endl;

                                                Room::SetClientLogin(login, c);

                                                char *cb = "SetClientLogin";
                                                this->Callback(c,cb);

                                        }

    }
    else{ // message
        cout << "1: " << s <<endl;
        string clientId = s.substr(0, pos);
        int cid = atoi(clientId.c_str());
        s.erase(0, pos + delimiter.length());
        cout << "2: " << s <<endl;
        pos = s.find(delimiter);
        string roomId = s.substr(0, pos);
        int rId = atoi( roomId.c_str() );

        //int rid = atoi(roomId.c_str());
        //content
        cout << "3: " << s << " clientId " << clientId <<" roomId "<< roomId << endl;
        s.erase(0, pos + delimiter.length());
        cout << "4: " << s << endl;

        cout << "Przed AddMessage: c->id: " << c->id << endl;
        Message *m = AddMessage(c, s);
        this->Broadcast(m);
		//success
        return 0;
    }

}

int Room::FindClientIndex(int clientId)
{
    for(size_t i=0; i<server->clients.size(); i++)
    {
        if((Server::clients[i]->id) == clientId) return (int) i;
    }
    cerr << "Client id not found." << endl;
    return -1;
}

void Room::ChangeRoom(int currentRoomId, int desiredRoomId, Client *c)
{
    cout << "Room::ChangeRoom c->id: " << c->id << endl;
    FindAndDeleteClientInCurrentRoom(currentRoomId, c);
    cout << "Room::ChangeRoom c->id: " << c->id << endl;
    Room::JoinRoom(desiredRoomId, c);
    cout << "Room::ChangeRoom c->id: " << c->id << endl;
}

void Room::FindAndDeleteClientInCurrentRoom(int currentRoomId, Client *c)
{
    cout << "Begin of FindAndDeleteClientInCurrentRoom" << endl;
    MyThread::LockMutex((const char *) c->name);
    currentRoomId = this->id;
cout << "Room::FindAndDeleteClientInCurrentRoom c->id: " << c->id << endl;



            //server->rooms[currentRoomId]->fds[server->rooms[currentRoomId]->clientCount-1].fd = -1;


            //close(server->rooms[currentRoomId]->fds[c->roomFd].fd);
            server->rooms[currentRoomId]->fds[c->roomFd].fd = -1;
            //fds[c->roomFd].events = 0;


            ////server->rooms[currentRoomId]->fds[server->rooms[currentRoomId]->clientCount-1].events = -1;
            ////server->rooms[currentRoomId]->fds[server->rooms[currentRoomId]->clientCount-1].revents = 0;

    for (int i=0; i < server->rooms.size(); i++)
    {
        if (server->rooms[i]->id == currentRoomId)
        {
            cout << "Specificated server found" << endl;
            cout << i <<"1iRoom::FindAndDeleteClientInCurrentRoom c->id: " << c->id << endl;

            for (int j=0; j < server->rooms[i]->clients.size(); j++)
            {
                if (server->rooms[i]->clients[j]->id == c->id)
                {
//                    for (int q1=0; q1<server->rooms[i]->clients.size(); q1++){
//                        cout << "q1: " <<  q1 <<" server->clients: " << server->rooms[i]->clients[q1]->id  << " socket: " << server->clients[q1]->sock << endl;
//                    }

//                    cout << j <<"1jRoom::FindAndDeleteClientInCurrentRoom c->id: " << c->id << " name:  " << c->name << " sock:  " << c->sock << endl;
//                    cout << "deleting client: " << c->id << " from room: " <<  server->rooms[i] << " id: " << currentRoomId << endl;
//                    //std::vector<Client>::iterator iter = std::find(rooms[i].clients.begin() + j);
//                    cout << "server->rooms[i]->clients.size(): " <<server->rooms[i]->clients.size() << endl;
//
//                    cout << endl;
                    //server->rooms[i]->clients.erase(server->rooms[i]->clients.begin() + j);
                    //server->rooms[i]->clientCount--;
                    Client* dummy = new Client();
                    dummy->SetId(-1);
                    server->rooms[i]->clients[j] = dummy;

//                    cout << j <<"2jRoom::FindAndDeleteClientInCurrentRoom c->id: " << c->id << " name:  " << c->name << " sock:  " << std::to_string(c->sock) << endl;
//                    for (int w=0; w<server->clients.size(); w++){
//                        cout <<"w: " << w <<" server->clients: " << server->clients[w]->id << server->clients[w]->sock << endl;
//                    }
//
//                    for (int q2=0; q2<server->rooms[i]->clients.size(); q2++){
//                        cout <<"q2: " << q2 <<" server->clients: " << server->rooms[i]->clients[q2]->id  << server->clients[q2]->sock  << " socket: " << server->clients[q1]->sock << endl;
//                    }
                    break;
                }
            }
            //cout << i << "2iRoom::FindAndDeleteClientInCurrentRoom c->id: " << c->id << endl;
        }
    }
    //cout << "endRoom::FindAndDeleteClientInCurrentRoom c->id: " << c->id << endl;
    MyThread::UnlockMutex((const char *) c->name);
}

void Room::JoinRoom(int id, Client *c)
{
    cout << "Begin of joining client" << endl;
    MyThread::LockMutex((const char *) c->name);
    int index = -1;
    for (int i=0; i < server->rooms.size(); i++)
    {
        if (server->rooms[i]->id == id)
        {
            index = i;
            cout << "Specificated server found" << endl;
            bool flag = true;
            //sprawdz czy server->rooms.clients ma juz takiego clienta

            for (int j=0; j < server->rooms[i]->clients.size(); j++)
            {
                if (server->rooms[i]->clients[j]->id == c->id)
                {
                    cout << "Client already exists" << endl;
                    flag = false;
                    break;
                }
            }

            if (flag)
            {
                cout << "adding client: " << c->id << " to room: " <<  server->rooms[i] << " id: " << id << endl;
                server->rooms[i]->clients.push_back(c);
                server->rooms[i]->clientCount++;
            }
        }
    }

    //cout << server->rooms[id]->clients.size() << endl;
    server->rooms[index]->fds[server->rooms[index]->clientCount-1].fd = c->sock;
    server->rooms[index]->fds[server->rooms[index]->clientCount-1].events = POLLIN;
    server->rooms[index]->fds[server->rooms[index]->clientCount-1].revents = 0;

    MyThread::UnlockMutex((const char *) c->name);
}

int Room::CreateRoom()
{
    MyThread::LockMutex((const char *) this->name);
    char buffer[256-25];
    Room *r = new Room();
    cout << "Room::CreateRoom() id: "<< server->roomCounter << endl;
    r->SetId(server->roomCounter);
    sprintf(buffer, "Room n.%d", r->id);
    r->SetName(buffer);
    server->roomCounter++;
    server->rooms.push_back(r);
    r->Start();
    MyThread::UnlockMutex((const char *) this->name);
    return r->id;
}



