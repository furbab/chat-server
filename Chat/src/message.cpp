#include "message.h"

using namespace std;

//Client Message::client;

Message::Message(int _clientId) {
  Message::clientId = id;
  time (&rawtime);
  //printf ("The current local time is: %s", ctime (&rawtime));
}

Message::Message(Client *c) {
  Message::owner = c;
  time (&rawtime);
  //printf ("The current local time is: %s", ctime (&rawtime));
}

void Message::SetContent(string _content) {
    content = _content;
}

void Message::SetId(int id) {
  this->id = id;
}

void Message::SetClientId(int id) {
  this->clientId = id;
}

void Message::SetRoomId(int id) {
  this->roomId = id;
}

void Message::SetClientLogin(string login) {
  clientLogin = login;
}
