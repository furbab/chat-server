#include "server.h"
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <arpa/inet.h>

using namespace std;

//Actually allocate clients
vector<Client*> Server::clients;
vector<Room*> Server::rooms;
int Server::clientCounter;
int Server::roomCounter;


Server::Server()
{

    //Initialize static mutex from MyThread
    MyThread::InitMutex();

    //For setsock opt (REUSEADDR)
    int yes = 1;

    clientCounter = 0;
    roomCounter= 0;

    //Init serverSock and start listen()'ing
    serverSock = socket(AF_INET, SOCK_STREAM, 0);
    memset(&serverAddr, 0, sizeof(sockaddr_in));
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_addr.s_addr = INADDR_ANY;//inet_addr("10.0.2.15");//INADDR_ANY;//inet_addr("192.168.56.101");//INADDR_ANY;
    serverAddr.sin_port = htons(PORT1);

    //Create first room
    Server::CreateInitialRoom();

    //Avoid bind error if the socket was not close()'d last time;
    setsockopt(serverSock,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(int));

    if(bind(serverSock, (struct sockaddr *) &serverAddr, sizeof(sockaddr_in)) < 0)
        cerr << "Failed to bind";

    if(listen(serverSock, 5) < 0)
        cerr << "Failed to listen";

}

/*
	AcceptAndDispatch();

	Main loop:
		Blocks at accept(), until a new connection arrives.
		When it happens, create a new thread to handle the new client.
*/
void Server::AcceptAndDispatch()
{
    Client *c;

    socklen_t cliSize = sizeof(sockaddr_in);
    char buffer[256-25], message[256];
    while(1)
    {

        cout << "Waiting for connection" << endl;
        c = new Client();
        MyThread::LockMutex((const char *) c->name);
        //Before adding the new client, calculate its id. (Now we have the lock)
        c->SetId(clientCounter);
        clientCounter++;
        sprintf(buffer, "Client n.%d", c->id);
        c->SetName(buffer);
        cout << "Adding client with id: " << c->id << endl;
        Server::clients.push_back(c);

        MyThread::UnlockMutex((const char *) c->name);
        //Blocks here;
        c->sock = accept(serverSock, (struct sockaddr *) &clientAddr, &cliSize);
        cout << c->sock << endl;
        if(c->sock < 0)
        {
            cerr << "Error on accept";
        }

        else
        {
            cout << "Client accepted" << endl;
            Server::JoinRoom(Server::rooms[0]->id, c);
            //prepare poll array field for new client
            Server::rooms[0]->fds[Server::rooms[0]->clientCount-1].fd = c->sock;
            Server::rooms[0]->fds[Server::rooms[0]->clientCount-1].events = POLLIN;
            Server::rooms[0]->fds[Server::rooms[0]->clientCount-1].revents = 0;
        }
    //   Server::rooms[0].Start();
    }
}


//char* Server::PrepareMessage(int roomId, char *message)
//{
//    char *rId;
//    sprintf(rId, "%i", roomId);
//    const char* delimiter= "|||";
//
//    char* full_text;
//    //cout << strlen(rId) << endl;
//    //cout << strlen(delimiter) << endl;
//    //cout << strlen(message) << endl;
//    //full_text= malloc(5+3+strlen(message)+1);
//    strcpy(full_text, rId );
//    strcat(full_text, delimiter);
//    strcat(full_text, message);
//
//    return full_text;
//
//}

char* Server::PrepareMessage(int roomId, char* message)
{
    char *rId;
    sprintf(rId, "%i", roomId);
    char* delimiter= "|||";

    char* full_text;
    //cout << strlen(rId) << endl;
    //cout << strlen(delimiter) << endl;
    //cout << strlen(message) << endl;
    full_text= (char*)malloc(strlen(rId)+strlen(delimiter)+strlen(message)+1);
    strcpy(full_text, rId );
    strcat(full_text, delimiter);
    strcat(full_text, message);



    return full_text;

}

void Server::SendToRoom(int roomId, int clientId, char *message)
{
    int n;

    //Acquire the lock
    MyThread::LockMutex("'SendToAll()'");
    for (int i=0; i < Server::rooms.size(); i++)
    {
        if (Server::rooms[i]->id == roomId)
        {

            for(size_t j=0; j<rooms[i]->clients.size(); j++)
            {
                if(rooms[i]->clients[j]->id != clientId)
                {
                    n = send(Server::rooms[i]->clients[j]->sock, message, strlen(message), 0);
                    cout << n << " bytes sent." << endl;
                }

            }
        }
    }
    //Release the lock
    MyThread::UnlockMutex("'SendToAll()'");
}

void Server::SendToAll(char *message)
{
    int n;

    //Acquire the lock
    MyThread::LockMutex("'SendToAll()'");

    for(size_t i=0; i<clients.size(); i++)
    {
        n = send(Server::clients[i]->sock, message, strlen(message), 0);
        cout << n << " bytes sent." << endl;
    }

    //Release the lock
    MyThread::UnlockMutex("'SendToAll()'");
}

//void Server::ListClients()
//{
//    for(size_t i=0; i<clients.size(); i++)
//    {
//        cout << clients.at(i).name << endl;
//    }
//}

/*
  Should be called when vector<Client> clients is locked!
*/
int Server::FindClientIndex(Client *c)
{
    for(size_t i=0; i<clients.size(); i++)
    {
        if((Server::clients[i]->id) == c->id) return (int) i;
    }
    cerr << "Client id not found." << endl;
    return -1;
}

int Server::CreateInitialRoom()
{
    MyThread::LockMutex("'CreateInitialRoom()'");
    char buffer[256-25];
    Room *r = new Room();
    r->SetId(roomCounter);
    sprintf(buffer, "Room n.%d", r->id);
    r->SetName(buffer);
    roomCounter++;
    rooms.push_back(r);
    r->Start();
    MyThread::UnlockMutex("'CreateInitialRoom()'");
    return r->id;
}

void Server::JoinRoom(int id, Client *c)
{
    cout << "Begin of joining client" << endl;
    MyThread::LockMutex((const char *) c->name);
    for (int i=0; i < Server::rooms.size(); i++)
    {
        if (Server::rooms[i]->id == id)
        {
            cout << "Specificated server found" << endl;
            bool flag = true;
            //sprawdz czy Server::rooms.clients ma juz takiego clienta

            for (int j=0; j < Server::rooms[i]->clients.size(); j++)
            {
                if (Server::rooms[i]->clients[j]->id == c->id)
                {
                    cout << "Client already exists" << endl;
                    flag = false;
                    break;
                }
            }

            if (flag)
            {
                cout << "adding client to room: " <<  Server::rooms[i] << " id: " << id << endl;
                Server::rooms[i]->clients.push_back(c);
                Server::rooms[i]->clientCount++;
            }
        }
    }
    MyThread::UnlockMutex((const char *) c->name);
}
