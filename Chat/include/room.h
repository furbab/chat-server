#ifndef _room_h_
#define _room_h_

#include <iostream>
#include <stdio.h>
#include <string.h>
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <sys/poll.h>

#include <sys/socket.h>
#include <netinet/in.h>


#include "client.h"
#include "message.h"
#include "mythread.h"

#define MAX_NAME_LENGHT 20
#define MAX_FDS 20
#define POLL_TIMEOUT 5000

using namespace std;

class Room {
  public:
    char *name;
    int clientCount;
    int messageCount;
    int id;
    MyThread* thread;
	vector<Client*> clients;
	vector<Message> Messages;
	struct pollfd fds[MAX_FDS];

  public:
    Room();
    char* PrepareMessage(int roomId, char* message);
    void SetName(const char *name);
    void SetId(int id);
    void PollClients();
    void Start();
    static void* PollWrapper(void *ptr);
	Message* AddMessage(Client *c, string s);
	int ProcessMessage(string s, Client *c);
	void Broadcast(Message *m);
	void Callback(Client *c, const char *message);
    void SendHistory(Client *c);
    int FindClientIndex(int clientId);
    void SendRooms(Client *c);
    void GetId(Client *c);
    void GetRoomId(Client *c);
    void SetClientLogin(string login, Client *c);

	int CreateRoom();
	void JoinRoom(int id, Client *c);
	void FindAndDeleteClientInCurrentRoom(int currentRoomId, Client *c);
	void ChangeRoom(int currentRoomId, int desiredRoomId, Client *c);
};

#endif
