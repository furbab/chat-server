#ifndef _server_h_
#define _server_h_

#include <iostream>
#include <vector>
#include <stdio.h>
#include <string.h>
#include <cstdio>
#include <cstdlib>
#include <cstring>

#include <stdio.h>
#include <stdlib.h>

#include <sys/socket.h>
#include <netinet/in.h>

#include "mythread.h"
#include "client.h"
#include "room.h"

#define PORT1 30666
#define PORT2 30666

using namespace std;

class Server {

  public:
    static vector<Client*> clients;
	static vector<Room*> rooms;
    static int clientCounter, roomCounter;
    //Socket stuff
    int serverSock, clientSock;
    struct sockaddr_in serverAddr, clientAddr;
    char buff[256];

  public:
    Server();
    void AcceptAndDispatch();
    static void * HandleClient(void *args);

  private:
    static void ListClients();
	static void SendToRoom(int roomId, int clientId, char *message);
    static void SendToAll(char *message);
    static int FindClientIndex(Client *c);

	//static void CreateRoom(char *name, Client *c);
	static int CreateInitialRoom();
	static void JoinRoom(int roomId, Client *c);
	static void DeleteRoom();

	static int FindRoom(int roomId, Client *c);

	static void AddMessage(int roomId, Client *c, string s);
	static int ProcessMessage(string s, Client *c);

	static char* PrepareMessage(int roomId, char* message);


};

#endif
