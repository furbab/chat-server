#ifndef _message_h_
#define _message_h_

#include <iostream>
#include <stdio.h>
#include <string.h>
#include <cstdio>
#include <cstdlib>
#include <vector>

#include "client.h"

using namespace std;

class Message {
  public:
    string content;
    int id;
	int clientId;
	int roomId;
	string clientLogin;
	Client *owner;
    time_t rawtime;

  public:
    Message();
    Message(int _clientId);
    Message(Client *c);
    void SetContent(string _content);
    void SetId(int id);
    void SetClientId(int id);
    void SetRoomId(int id);
    void SetClientLogin(string login);
};

#endif
