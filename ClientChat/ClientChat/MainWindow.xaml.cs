﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClientChat
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {

        public bool mutex { get; set; } = true;
        public bool connected { get; set; } = false;

        IPAddress ipAddress;
        IPEndPoint remoteEP;
        Socket sender;
        public string newRoom
        {
            get { return _newRoom; }

            set
            {
                _newRoom = value;
                //lblRoom.Content = "Room: "+ _newRoom;
                lblRoomId.Text = "Room: " + _newRoom;
                var slowTask1 = Task.Factory.StartNew(() => {

                    this.Dispatcher.Invoke(new Action(async () =>
                    {
                        txtblkMessages.Text = string.Empty;
                        await SendToRoomAsync("$$$SendHistory&&");
                    }));
                });
                NotifyPropertyChanged("newRoom");
            }
        }
        private string _roomId = "Room: ";
        int _clientId;
        string _login;
        RoomPopup roompopup;
        public ObservableCollection<string> RoomsList { get; set; } = new ObservableCollection<string>();

        private string _sendMessage;
        public string SendMessage
        {
            get { return _sendMessage; }

            set
            {
                _sendMessage = value;
                NotifyPropertyChanged("SendMessage");
            }
        }

        public string RoomId
        {
            get { return _roomId; }

            set
            {
                _roomId = value;
                NotifyPropertyChanged("RoomId");
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            txtSend.DataContext = this;
            //ipAddress = IPAddress.Loopback;// ipHostInfo.AddressList[0];
            ipAddress = IPAddress.Parse("127.0.0.1");//IPAddress.Loopback;
            remoteEP = new IPEndPoint(ipAddress, 30666);
            sender = new Socket(ipAddress.AddressFamily,
                SocketType.Stream, ProtocolType.Tcp);
            
        }





        private void ListenOnServerAsync()
        {

            // Connect to a remote device.  
            try
            {
                // Establish the remote endpoint for the socket.  
                // This example uses port 11000 on the local computer.  
                //IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());


                // Create a TCP/IP  socket.  
                //ipAddress = IPAddress.Parse("192.168.71.3");//IPAddress.Loopback;
                remoteEP = new IPEndPoint(ipAddress, 30666);
                sender = new Socket(ipAddress.AddressFamily,
                    SocketType.Stream, ProtocolType.Tcp);

                // Connect the socket to the remote endpoint. Catch any errors.  
                try
                {

                    byte[] bytes = new byte[1024];

                    sender.Connect(remoteEP);

                    Console.WriteLine("Socket connected to {0}",
                        sender.RemoteEndPoint.ToString());
                    var lastmsg = string.Empty;
                    int counter = 0;
                    while (sender.IsConnected() && counter < 10)//&& counter < 10
                    {
                        connected = true;
                        int bytesRec = sender.Receive(bytes);
                        var msg = Encoding.ASCII.GetString(bytes, 0, bytesRec);
                        Console.WriteLine("Echoed test = {0}", msg);

                        if (string.IsNullOrWhiteSpace(msg)) counter++;
                        if (lastmsg != msg) ParseMessage(msg);
                        lastmsg = msg;

                        //System.Threading.Thread.Sleep(1000);
                    }

                    connected = false;
                    //Release the socket.  
                    sender.Shutdown(SocketShutdown.Both);
                    sender.Close();
                    
                    var slowTask1 = Task.Factory.StartNew(() => {

                        this.Dispatcher.Invoke(new Action(async () =>
                        {
                            DeInitialize();
                        }));
                    });


                }
                catch (ArgumentNullException ane)
                {

                    Console.WriteLine("ArgumentNullException : {0}", ane.ToString());
                    MessageBox.Show($"ListenOnServerAsync failed, ex: {ane.ToString()}", "Error");
                    var slowTask1 = Task.Factory.StartNew(() => {

                        this.Dispatcher.Invoke(new Action(async () =>
                        {
                            DeInitialize();
                        }));
                    });
                }
                catch (SocketException se)
                {
                    Console.WriteLine("SocketException : {0}", se.ToString());
                    MessageBox.Show($"ListenOnServerAsync failed, ex: {se.ToString()}", "Error");
                    var slowTask1 = Task.Factory.StartNew(() => {

                        this.Dispatcher.Invoke(new Action(async () =>
                        {
                            DeInitialize();
                        }));
                    });
                }
                catch (Exception e)
                {
                    Console.WriteLine("Unexpected exception : {0}", e.ToString());
                    MessageBox.Show($"ListenOnServerAsync failed, ex: {e}", "Error");
                    var slowTask1 = Task.Factory.StartNew(() => {

                        this.Dispatcher.Invoke(new Action(async () =>
                        {
                            DeInitialize();
                        }));
                    });
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                MessageBox.Show($"ListenOnServerAsync failed, ex: {e.ToString()}", "Error");
                var slowTask1 = Task.Factory.StartNew(() => {

                    this.Dispatcher.Invoke(new Action(async () =>
                    {
                        DeInitialize();
                    }));
                });
            }





            //byte[] bytes = new byte[1024];

            //sender.Connect(remoteEP);

            //Console.WriteLine("Socket connected to {0}",
            //    sender.RemoteEndPoint.ToString());
            //var lastmsg = string.Empty;
            ////int counter = 0;
            //while (sender.IsConnected())//&& counter < 10
            //{

            //    int bytesRec = sender.Receive(bytes);
            //    var msg = Encoding.ASCII.GetString(bytes, 0, bytesRec);
            //    Console.WriteLine("Echoed test = {0}",msg);

            //    //if (string.IsNullOrWhiteSpace(msg)) counter++;
            //    if (lastmsg != msg) ParseMessage(msg);
            //    lastmsg = msg;

            //    //System.Threading.Thread.Sleep(1000);
            //}

            ////Release the socket.  
            //sender.Shutdown(SocketShutdown.Both);
            //sender.Close();
            //DeInitialize();



            //// Connect to a remote device.  
            //try
            //{
            //    // Establish the remote endpoint for the socket.  
            //    // This example uses port 11000 on the local computer.  
            //    //IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());


            //    // Create a TCP/IP  socket.  


            //    // Connect the socket to the remote endpoint. Catch any errors.  
            //    try
            //    {
            //        sender.Connect(remoteEP);

            //        Console.WriteLine("Socket connected to {0}",
            //            sender.RemoteEndPoint.ToString());

            //        //// Encode the data string into a byte array.  
            //        //byte[] msg = Encoding.ASCII.GetBytes("0|||test");

            //        //// Send the data through the socket.  
            //        //int bytesSent = sender.Send(msg);
            //        while (true)
            //        {
            //            // Receive the response from the remote device.  
            //            int bytesRec = sender.Receive(bytes);
            //            Console.WriteLine("Echoed test = {0}",
            //                Encoding.ASCII.GetString(bytes, 0, bytesRec));

            //            // Release the socket.  
            //            //sender.Shutdown(SocketShutdown.Both);
            //            //sender.Close();
            //        }


            //    }
            //    catch (ArgumentNullException ane)
            //    {
            //        Console.WriteLine("ArgumentNullException : {0}", ane.ToString());
            //    }
            //    catch (SocketException se)
            //    {
            //        Console.WriteLine("SocketException : {0}", se.ToString());
            //    }
            //    catch (Exception e)
            //    {
            //        Console.WriteLine("Unexpected exception : {0}", e.ToString());
            //    }

            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine(e.ToString());
            //}
        }

        private void ParseMessage(string msg)
        {
            if (msg.Contains("GetId@@@"))
            {
                var t = (msg.Substring(msg.IndexOf("GetId@@@") + "GetId@@@".Length)).Split(new string[] { "\n", "###" }, StringSplitOptions.None);
                _clientId = Int32.Parse(t[0]);

            }
            if (msg.Contains("GetRoomId@@@"))
            {
                //_roomId = Int32.Parse(msg.Remove(0, "GetRoomId@@@".Length));
                //lblRoomId.Dispatcher.Invoke(new Action(() => { lblRoomId.Content = _roomId.ToString(); }));
                var t = (msg.Substring(msg.IndexOf("GetRoomId@@@") + "GetRoomId@@@".Length)).Split(new string[] { "\n", "###" }, StringSplitOptions.None);
                RoomId = t[0];

                txtblkMessages.Dispatcher.Invoke(new Action(() =>
                {
                    //lblRoom.Content = "Room: " + RoomId;
                    lblRoomId.Text = "Room: " + RoomId;
                }));

            }

            if (msg.Contains("SendingRooms@@@"))
            {
                //_roomId = Int32.Parse(msg.Remove(0, "GetRoomId@@@".Length));
                //lblRoomId.Dispatcher.Invoke(new Action(() => { lblRoomId.Content = _roomId.ToString(); }));
                var r1 = (msg.Substring(msg.IndexOf("SendingRooms@@@") + "SendingRooms@@@".Length));
                if (r1.Count() > 0)
                {
                    var rooms = r1.Split(new string[] { "@@@", "\n", "###" }, StringSplitOptions.RemoveEmptyEntries);
                    

                    txtblkMessages.Dispatcher.Invoke(new Action(() =>
                    {
                        RoomsList.Clear();

                    }));
                    foreach ( var room in rooms)
                    {
                        txtblkMessages.Dispatcher.Invoke(new Action(() =>
                        {
                            RoomsList.Add(room);

                        }));
                    }
                }
            }
            if (msg.Contains("SendingHistory"))
            {
                txtblkMessages.Dispatcher.Invoke(new Action(() => txtblkMessages.Text = string.Empty));
                
                var h1 = (msg.Substring(msg.IndexOf("SendingHistory") + "SendingHistory".Length));
                if (h1.Count() >= 4 && h1.IndexOf("###") > 0)
                {
                    var h15 = h1.Split(new string[] { "###" }, StringSplitOptions.RemoveEmptyEntries);
                    var h2 = h15[0].Split(new string[] { "@@@", "\n"}, StringSplitOptions.RemoveEmptyEntries);
                    if (h2.Count() >= 4)
                        for (int i = 0; i < h2.Count(); i += 5)
                        {
                            txtblkMessages.Dispatcher.Invoke(new Action(() =>
                            {
                                if (h2[i + 2] == _login)
                                {
                                    txtblkMessages.Text += h2[3] + "\n" + h2[i + 2] + ": " + h2[i + 4] + "\n";
                                    //txtblkMessages.Text += "                                                                            " + h2[i + 2] + ": " + h2[i + 3] + "\n";

                                    Console.WriteLine("                                   " + h2[i + 2] + ": " + h2[i + 4] + "\n");
                                }
                                else
                                {
                                    txtblkMessages.Text += h2[3] + "\n" + h2[i + 2] + ": " + h2[i + 4] + "\n";
                                    Console.WriteLine(h2[i + 2] + ": " + h2[i + 4] + "\n");
                                }

                            }));
                        }


                    //txtblkMessages.Dispatcher.Invoke(new Action(() => {
                    //        txtblkMessages.Text += msg+"\n";


                    //    //txtblkMessages.Text += msg[4] + ": " +msg[5] + "\n";

                    //}));
                }


            }

            if (msg.Contains("RoomChanged"))
            {
                RoomId = newRoom;
                txtblkMessages.Dispatcher.Invoke(new Action(() =>
                {
                    //lblRoom.Content = "Room: " + RoomId;
                    lblRoomId.Text = "Room: " + RoomId;
                }));
            }
            if (msg.Contains("|||"))
            {
                if (!string.IsNullOrWhiteSpace(msg))
                {
                    txtblkMessages.Dispatcher.Invoke(new Action(() => {
                        var m = msg.Split(new string[] { "###", "|||" }, StringSplitOptions.None);

                        if (m[2].ToString() == _login)
                        {
                            //txtblkMessages.Text += "                                                                            " + m[2] + ": " + m[4] + "\n";
                            txtblkMessages.Text += m[4] + m[2] + ": " + m[5] + "\n";

                        }
                        else
                        {
                            //txtblkMessages.Text += m[2] + ": " + m[4] + m[5] + "\n";
                            txtblkMessages.Text += m[4] + m[2] + ": " + m[5] + "\n";
                        }

                        //txtblkMessages.Text += msg[4] + ": " +msg[5] + "\n";

                    }));
                }
                mutex = true;
            }

            if (msg.Contains("MessageReceived") || msg.Contains("HistorySent") || msg.Contains("GetIdSent") || msg.Contains("GetRoomIdSent")
                || msg.Contains("RoomCreated") || msg.Contains("RoomsListSent") || msg.Contains("SetClientLogin") || msg.Contains("ChangedRoom"))
            {
                mutex = true;

            }

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private async void btnLogin_Click(object sender, RoutedEventArgs e)
        {

            Initialize();
        }

        private void DeInitialize()
        {
            txtLogin.IsEnabled = true;
            btnLogin.IsEnabled = true;

            btnChangeRoom.IsEnabled = false;

            fieldMessages.IsEnabled = false;
            txtblkMessages.IsEnabled = false;
            btnCreateRoom.IsEnabled = false;

            txtSend.IsEnabled = false;
            btnSend.IsEnabled = false;

            //lblRoom.IsEnabled = false;
            lblRoomId.IsEnabled = false;
        }

        private async Task Initialize()
        {
            if (!string.IsNullOrWhiteSpace(txtLogin.Text))
            {
                _login = txtLogin.Text;
                var request = SendToServerAsync();
                bool returnCode = await request;

                if (returnCode)
                {
                    txtLogin.IsEnabled = false;
                    btnLogin.IsEnabled = false;

                    btnCreateRoom.IsEnabled = true;

                    fieldMessages.IsEnabled = true;
                    txtblkMessages.IsEnabled = true;
                    btnChangeRoom.IsEnabled = true;

                    txtSend.IsEnabled = true;
                    btnSend.IsEnabled = true;

                    //lblRoom.IsEnabled = true;
                    lblRoomId.IsEnabled = true;

                    try
                    {
                        var slowTask0 = Task.Factory.StartNew(async () => {
                            var slowTask1 = Task.Factory.StartNew(() => ListenOnServerAsync());
                            //await slowTask1;
                            while (!connected)
                            {
                                Console.WriteLine("Not connected");
                                await Task.Delay(25);
                            }
                            var slowTask2 = Task.Factory.StartNew(() => {

                                SendToRoomAsync("$$$SetClientLogin" + _login + "&&");
                                SendToRoomAsync("$$$GetId&&");
                                SendToRoomAsync("$$$GetRoomId&&");
                                SendToRoomAsync("$$$SendHistory&&");
                            });

                        });

                        //var slowTask3 = Task.Factory.StartNew(() => SendToRoomAsync("$$$GetId"));
                        //var slowTask4 = Task.Factory.StartNew(() => SendToRoomAsync("$$$GetRoomId"));
                        //var slowTask5 = Task.Factory.StartNew(() => SendToRoomAsync("$$$SendHistory"));
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show($"Initialize failed, ex: {e.ToString()}", "Error");
                        DeInitialize();
                    }

                }
                else
                {
                    MessageBox.Show("Login failed!", "Error");
                }
            }
            else
            {
                MessageBox.Show("Enter login!", "Error");
            }
        }

        public async Task SendToRoomAsync(string message)
        {
            try
            {
                while (!mutex)
                {
                    Console.WriteLine($"is busy {message}");
                    await Task.Delay(25);
                }

                if (mutex)
                {
                    mutex = false;

                    byte[] msg = Encoding.ASCII.GetBytes(message);

                    int bytesSent = sender.Send(msg);
                    Console.WriteLine(message);
                    Console.WriteLine($"Bytes sent: {bytesSent}");
                    if (bytesSent == 0)
                    {
                        await SendToRoomAsync(message);
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show($"SendToRoomAsync failed, ex: {e.ToString()}", "Error");
                DeInitialize();
            }


        }

        private async Task<bool> SendToServerAsync()
        {
            return true;
        }

        private async void btnSend_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(SendMessage))
            {
                //var slowTask = Task.Factory.StartNew(() => SendToRoomAsync("0|||test"));
                var toSend = "0|||0|||" + SendMessage + "&&";
                var slowTask = Task.Factory.StartNew(() => SendToRoomAsync(toSend));
                SendMessage = string.Empty;
            }

        }

        private void fieldMessages_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            // User scroll event : set or unset auto-scroll mode
            if (e.ExtentHeightChange == 0)
            {   // Content unchanged : user scroll event
                if (fieldMessages.VerticalOffset == fieldMessages.ScrollableHeight)
                {   // Scroll bar is in bottom
                    // Set auto-scroll mode
                    AutoScroll = true;
                }
                else
                {   // Scroll bar isn't in bottom
                    // Unset auto-scroll mode
                    AutoScroll = false;
                }
            }

            // Content scroll event : auto-scroll eventually
            if (AutoScroll && e.ExtentHeightChange != 0)
            {   // Content changed and auto-scroll mode set
                // Autoscroll
                fieldMessages.ScrollToVerticalOffset(fieldMessages.ExtentHeight);
            }
        }


        private Boolean AutoScroll = true;
        private string _newRoom;

        // Property Change Logic    
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void btnChangeRoom_Click(object sender, RoutedEventArgs e)
        {
            //RoomPopup rp2 = new RoomPopup(RoomId, RoomsList, this);
            //rp2.Show();
            var slowTask1 = Task.Factory.StartNew(async () => {
                await SendToRoomAsync("$$$SendRooms&&");
                txtblkMessages.Dispatcher.Invoke(new Action(() => {
                    roompopup = new RoomPopup(this);
                    roompopup.currentRoom = RoomId;
                    roompopup.listRoom.ItemsSource = RoomsList;
                    roompopup.Show();

                }));
            });

        }

        private void btnCreateRoom_Click(object sender, RoutedEventArgs e)
        {
            var slowTask1 = Task.Factory.StartNew(async () => {
                await SendToRoomAsync("$$$CreateRoom&&");
            });
        }
    }
}
