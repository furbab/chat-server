﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientChat
{
    /// <summary>
    /// Interaction logic for RoomPopup.xaml
    /// </summary>
    public partial class RoomPopup : Window
    {


        public ObservableCollection<string> list { get; set; }
        public MainWindow MW { get; set; }
        public string currentRoom { get; set; }

        public RoomPopup(MainWindow mainWindow)
        {
            InitializeComponent();
            MW = mainWindow;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if(listRoom.SelectedItem != null&& listRoom.SelectedItem.ToString() != currentRoom)
            {
                var slowTask1 = Task.Factory.StartNew(() => {

                    this.Dispatcher.Invoke(new Action(async () =>
                    {
                        Console.WriteLine(listRoom.SelectedValue.ToString());
                        Console.WriteLine(listRoom.SelectedItem.ToString());
                        MW.newRoom = listRoom.SelectedValue.ToString();
                        await MW.SendToRoomAsync("$$$ChangeRoom" + currentRoom + "___" + listRoom.SelectedValue.ToString() + "&&");
                        MW.RoomId = listRoom.SelectedValue.ToString();
                        this.Close();
                    }));
                });
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
